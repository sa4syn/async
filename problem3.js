const fetch = require('node-fetch');
const fs = require('fs');

function getAllUserByName(userName, userData) {
  let searchPattern = new RegExp(userName, 'gi');
  return userData.filter(user => {
    return searchPattern.test(user['name']);
  })
};

async function groupTodoByUser(urlTodos) {
try{
  let todoList = await fetch(urlTodos).then((response) => response.json());
  return todoList.reduce((groupedTodo, todo) => {
    if (!groupedTodo[todo['userId']]) {
      groupedTodo[todo['userId']] = [];
    }
    groupedTodo[todo['userId']].push(todo['title']);
    return groupedTodo
  }, {})
}catch(err){
  console.log('error occured');
  return [];
}
}

function getUserTodoByName(urlUsers, urlTodos) {
  return fetch(urlUsers)
    .then((urlResponse) => urlResponse.json())
    .then((users) => {
      return groupTodoByUser(urlTodos).then((groupedTodoByUser) => {
        const matchedUsers = getAllUserByName('Nicholas', users);
        return matchedUsers.reduce((allMatchedUser, user) => {
          if (!groupedTodoByUser[user['id']]) {
            return allMatchedUser;
          } else {
            allMatchedUser.push({ [user['id']]: groupedTodoByUser[user['id']] });
            return allMatchedUser;
          }
        }, [])
      })
    }).catch((err) => console.log('error occured'));
}


module.exports = { getUserTodoByName };