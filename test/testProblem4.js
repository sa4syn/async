const { makeAPICallForEach, makeAPICallOnce } = require("../problem4")

makeAPICallForEach(1, 2).then((result) => console.log(result));
makeAPICallOnce(3, 4).then((data) => console.log(data));