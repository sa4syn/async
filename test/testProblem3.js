const { getUserTodoByName } = require('../problem3.js');
const urlUsers = "https://jsonplaceholder.typicode.com/users";
const urlTodos = "https://jsonplaceholder.typicode.com/todos";

getUserTodoByName(urlUsers, urlTodos).then((result) => console.log(result));