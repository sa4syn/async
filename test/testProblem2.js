const { getPromiseToReadFile } = require('../problem2.js');

getPromiseToReadFile('./output/output.json', (fileContent) => Object.values((JSON.parse(fileContent))))
.then((result) => console.log(result))
.catch((err) => console.log(err));