const fetch = require('node-fetch');
const fs = require('fs');

function groupTodoById(todoList){
  return todoList.reduce((groupedTodo, todo) => {
    if(!groupedTodo[todo['userId']]){
      groupedTodo[todo['userId']] = [];
    }
    if(todo['completed']){
      groupedTodo[todo['userId']].push(todo['title']);
    } else {
      groupedTodo[todo['userId']].unshift(todo['title']);
    }
    return groupedTodo;
  }, {})
}

function groupTodoByStatus(todoList){
  return todoList.reduce((groupedTodo, todo) => {
    if(!groupedTodo[todo['userId']]){
      groupedTodo[todo['userId']] = {complete: [], incomplete: []};
    }
    if(todo['completed']){
      groupedTodo[todo['userId']]['complete'].push(todo['title']);
    } else {
      groupedTodo[todo['userId']]['incomplete'].push(todo['title']);
    }
    return groupedTodo;
  }, {})
}

function dumpJson(fileName, inputObject){
  fs.writeFile(fileName, JSON.stringify(inputObject), function (err) {
      if (err) {
        console.log(err);
      } else {
        console.log(fileName);
      }
    });
}

async function fetchAndGroupUsers(urlUsers, urlTodos){
  let dir = './output';
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir); 
}
  try {
    let todoList = await fetch(urlTodos).then((response) => response.json());
    fetch(urlUsers)
      .then(response => response.json())
      .then(parsedObject => {
        outputObject = {A : parsedObject,
        B: groupTodoByStatus(todoList),
        C: groupTodoById(todoList)}
        dumpJson(dir + '/output.json', outputObject);
      }).catch((err) => {
        console.log(err);
      });
  }
  catch(err) {
    console.log(err);
    dumpJson(dir + '/output.json', {});
  }

}

module.exports = { fetchAndGroupUsers }