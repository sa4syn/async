const urlUsers = "https://jsonplaceholder.typicode.com/users";
const urlTodos = "https://jsonplaceholder.typicode.com/todos";

module.exports = {urlUsers, urlTodos};