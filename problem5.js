const sayHelloWorld = () => {
    return new Promise((resolve, reject) => {
        window.setTimeout(() => {
            console.log('Hello World')
            resolve();
        }, 1000)
    })
}

(function executeSayHelloWorld () {
    sayHelloWorld().then(() => {
        console.log('Hey');
    })
    
})()