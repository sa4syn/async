const fetch = require('node-fetch');

function makeAPICallForEach(){
    return Promise.all(Object.values(arguments).map((userId) => {
      return fetch(`https://jsonplaceholder.typicode.com/users?id=${userId}`)
      .then((response) => response.json()
      .then((parsedData) => parsedData[0]))
    })).catch((err) => console.log('Task failed!'));
  }

 function makeAPICallOnce(){
  let ids = Object.values(arguments);
  let url = ids.reduce((finalUrl, id) => {
    return finalUrl += 'id=' + id + '&';
  }, 'https://jsonplaceholder.typicode.com/users?');
  return ids.length ? fetch(url)
  .then((urlResponse) => urlResponse.json())
  .catch((err) => new Error('task failed')) : [];
} 

module.exports = {makeAPICallForEach, makeAPICallOnce};