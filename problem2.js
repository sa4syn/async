const fs = require('fs');

function getPromiseToReadFile(filePath, transformData = (data) => data) {
    return new Promise((resolve, reject) => {
       fs.readFile(filePath, 'utf8', function (err, fileContent) {
         if(err){
           reject(new Error('File not found'));
         } else{
           resolve(transformData(fileContent));
         }
      });
    })
  }

module.exports = { getPromiseToReadFile };